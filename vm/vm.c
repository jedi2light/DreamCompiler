#include <stdio.h>
#include <stdlib.h>

#include <vmstack.h>
#include <core/io.h>

int
dreamvm_run(
    const char* filename
) {
    IOFile* file = core_io_open(filename, "rb");
    byte_t* code = core_io_read_bytes(file);
    core_io_close(file);
    size_t  len = file->length;

    printf("[DreamVM]: File length is %d\n", len);

    // Using DreamVM stack implementation, iterate
    // instruction by instruction
    // and execute a program

    return EXIT_SUCCESS;
}


/* DreamVM - entrypoint: get filename and run it */

int main(int argc, char* argv[]) {

    const char* filename;
    if (argc < 2) {
        printf("[DreamVM]: Try './binary.out'\n");
        filename = "binary.out";
    } else {
        printf("[DreamVM]: Try '%s'\n", filename);
        filename = argv[1];
    }

    printf("[DreamVM]: Running: '%s'\n", filename);

    return dreamvm_run(filename);
}