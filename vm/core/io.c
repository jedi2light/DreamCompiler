#include <stdio.h>
#include <stdlib.h>

#include <core/io.h>

IOFile* core_io_init(
    void
) {
    return malloc(sizeof(IOFile));
}

IOFile* core_io_open(
    const char* path,
    const char* mode
) {
    IOFile* file = core_io_init();
    file->fptr = fopen(path, mode);
    fseek(file->fptr, 0, SEEK_END);
    file->size = ftell(file->fptr);
    fseek(file->fptr, 0, SEEK_SET);
    return file;
}

void    core_io_close(
    IOFile*     file
) {
    fclose(file->fptr);
}

byte_t* core_io_read_bytes(
    IOFile*     file
) {
    byte_t* buff = (byte_t*) malloc(file->size);
    file->length = fread(buff,
                        sizeof(byte_t),
                        file->size, file->fptr);
    return buff;
}