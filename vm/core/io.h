#include <stdio.h>
#include <stdlib.h>

typedef unsigned char byte_t;

struct _IOFile {
    long   size;
    FILE*  fptr;
    size_t length;
};

typedef struct _IOFile IOFile;

IOFile* core_io_open(const char* path,
                     const char* mode);
void    core_io_close(IOFile* file);
byte_t* core_io_read_bytes(IOFile* file);