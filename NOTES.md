=========================================================
Code
(+ 2 (* 2 2))

Assembly
PushNumber 2
PushNumber 2
Call *
PushNumber 2
Call +
Return 0

=========================================================
Code
(prn "Hello, World")

Assembly
PushUUnicode Length("Hello, World") Bytes("Hello, World")
Call prn
Return 0