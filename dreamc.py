#!/usr/bin/env python3

import os
import sys
import argparse

import lang


def main(args: argparse.Namespace) -> int:

    source_path = args.input
    if not os.path.exists(source_path) or not os.path.isfile(source_path):
        print('dream compiler: error: no such file or directory:', source_path)
        return 1

    try:
        with open(source_path, 'r', encoding='utf-8') as source_file:
            source_code = source_file.read()
    except PermissionError:
        print('dream compiler error: cannot read: permission denied', source_path)
        return 1

    try:
        tokens = lang.Lexer(source_code).tokens
    except lang.lexer.Error as error:
        print(f'dream compiler error: '
              f'syntax error at: {source_path}:{error.position}: {error.reason}.')
        return 1

    tree = lang.Parser(tokens).tree

    if args.debug:
        print('Debug mode is enabled')
        tree.dump()

    bytecode = lang.Compiler(tree).bytecode

    if args.debug:
        print('Bytecode is:', bytecode.hex())

    output_path = args.output
    try:
        with open(output_path, 'wb') as binary_file:
            binary_file.write(bytecode)
    except PermissionError:
        print('dream compiler error: cannot write: permission denied', output_path)
        return 1
    except FileNotFoundError:
        print('dream compiler error: cannot write: no such directory', output_path)
        return 1

    return 0


if __name__ == '__main__':

    parser = argparse.ArgumentParser('Dream Compiler')
    parser.add_argument('-o', '--output', help='Output file', default='binary.out')
    parser.add_argument('input', help='Input file')
    parser.add_argument('--debug', help='Enable various debug prints', action='store_true', default=False)

    sys.exit(main(parser.parse_args()))
