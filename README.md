## Usage

### Pre-Requisites
1. Python 3 installed
2. GCC and GNU/Make installed
3. `$ make` (this will compile VM)

### Compile a program
```bash
./dreamc.py \
    tests/hello-world.dream \
    -o tests/hello-world.binary
```

### Run program on DreamVM
```bash
./dreamvm.bin ./hello-world.binary
```