.PHONY: vm

BIN = ./dreamvm.bin

VM_DEPS += vm/vm.c \
           vm/core/io.o

CC_FLAGS += -I./vm

vm: $(VM_DEPS)
	gcc $(CC_FLAGS) vm/core/io.o vm/vm.c -o $(BIN)

vm/core/io.o: vm/core/io.c vm/core/io.h
	gcc $(CC_FLAGS) -c vm/core/io.c -o vm/core/io.o
