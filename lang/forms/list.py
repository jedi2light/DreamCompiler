from typing import List

from lang.node import Node
from lang.literal import Literal
from lang.forms.form import Form

from lang.compiler.assembly import Assembly


class ListForm(Form):

    __slots__ = ('elements',)

    elements: List[Node]

    def __init__(self, elements: List[Node]) -> None:

        self.elements = elements

    def compile(self) -> bytearray:

        # TODO: Currently, we assume the the all the forms are function calls, so that
        #       the head of the list is the function name and the tail is the arguments

        head = self.elements[0]

        assert isinstance(head, Literal) and head.kind == Literal.Identifier, SyntaxError(
            'expected identifier as the head of the list')

        function_name = head.value

        arguments = self.elements[1:]

        function_arguments_bytecode = bytearray(*[argument.compile() for argument in arguments])

        return function_arguments_bytecode + Assembly.compile_a_function_call_for(function_name)
