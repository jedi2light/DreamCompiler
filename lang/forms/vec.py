from typing import List

from lang.node import Node
from lang.forms.form import Form


class VecForm(Form):

    __slots__ = ('elements',)

    elements: List[Node]

    def __init__(self, elements: List[Node]) -> None:

        self.elements = elements

    def compile(self) -> bytearray:
        """In the future, this method will return set of the byte code instructions"""
