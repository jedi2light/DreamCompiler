from typing import List

from lang.node import Node
from lang.forms.form import Form


class MapForm(Form):

    __slots__ = ('pairs',)

    pairs: List[List[Node]]

    def __init__(self, pairs: List[List[Node]]) -> None:

        self.pairs = pairs

    def compile(self) -> bytearray:
        """In the future, this method will return set of the byte code instructions"""
