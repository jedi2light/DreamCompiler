class Node:

    __slots__ = ()

    @staticmethod
    def indent(indent: int) -> str:

        return ' ' * indent if indent else ''

    def dump(self, indent: int = 0) -> None:

        print(f'{self.indent(indent)}{self.__class__.__name__} {{')

        for slot_name in self.__slots__:
            slot_value = getattr(self, slot_name)

            if isinstance(slot_value, list):
                for slot_element in slot_value:
                    slot_element.dump(indent + 2)
            else:
                print(f'{self.indent(indent + 2)}{slot_name}: {slot_value}')

        print(f'{self.indent(indent)}}}')

    def compile(self) -> bytearray:

        class_name = self.__class__.__name__
        raise NotImplementedError(f'{class_name}.compile() not implemented')
