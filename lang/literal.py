from typing import Optional

from lang.node import Node
from lang.token import Token

from lang.compiler.assembly import Assembly


class Literal(Node):

    __slots__ = ('kind', 'value')

    Nil: str = 'nil'
    Number: str = 'number'
    String: str = 'string'
    Boolean: str = 'boolean'
    Identifier: str = 'identifier'

    kind: str
    value: Optional[str]

    def __init__(self, kind: str, value: Optional[str]) -> None:

        self.kind = kind
        self.value = value

    @staticmethod
    def nil() -> 'Literal':

        return Literal(Literal.Nil, 'nil')

    @staticmethod
    def convert(token: Token) -> 'Literal':

        if token.is_number():
            return Literal(Literal.Number, token.value())
        elif token.is_string():
            return Literal(Literal.String, token.value())
        elif token.is_identifier():
            if token.value() == 'nil':
                return Literal(Literal.Nil)
            if token.value() in ['true', 'false']:
                return Literal(Literal.Boolean, token.value())
            else:
                return Literal(Literal.Identifier, token.value())

    def compile(self) -> bytearray:

        if self.kind == Literal.Number:
            as_int = int(self.value)
            return Assembly.compile_a_number_push_for(as_int)
        elif self.kind == Literal.String:
            return Assembly.compile_a_string_push_for(self.value)
        elif self.kind == Literal.Boolean:
            return Assembly.compile_a_number_push_for(1 if self.value == 'true' else 0)
        elif self.kind == Literal.Nil:
            return Assembly.compile_a_number_push_for(0)
        elif self.kind == Literal.Identifier:
            raise NotImplementedError('identifier literal compilation not implemented')
        else:
            raise NotImplementedError(f'literal compilation for {self.kind} not implemented')
