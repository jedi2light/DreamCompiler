from typing import List

from lang.node import Node
from lang.token import Token
from lang.forms import ListForm, MapForm, VecForm
from lang.literal import Literal


class Parser:

    _cursor: int
    _tokens: List[Token]
    _parsed: Node = None

    def __init__(self, tokens: List[Token]) -> None:

        self._cursor = 0
        self._tokens = tokens

    @property
    def tree(self):

        if not self._parsed:
            self._parse()
        return self._parsed

    def _parse(self) -> None:

        self._parsed = self._parse_node()

    def _advance(self) -> None:

        self._cursor += 1

    def _parse_node(self) -> Node:

        token = self._tokens[self._cursor]

        if token.is_a_literal():
            self._advance()
            return Literal.convert(token)

        if token.is_opening_paren():
            self._advance()
            return self._parse_list_form()

        # handle Vec-, and MapForm here as well

    def _parse_list_form(self) -> ListForm:

        elements = []

        while True:

            token = self._tokens[self._cursor]

            if token.is_closing_paren():
                return ListForm(elements)

            else:
                elements.append(self._parse_node())
