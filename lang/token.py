class Token:

    Number: str = 'Number'
    String: str = 'String'
    Identifier: str = 'Identifier'
    OpeningParen: str = 'OpeningParen'
    ClosingParen: str = 'ClosingParen'
    OpeningBracket: str = 'OpeningBracket'
    ClosingBracket: str = 'ClosingBracket'
    OpeningCurlyBrace: str = 'OpeningCurlyBrace'
    ClosingCurlyBrace: str = 'ClosingCurlyBrace'

    _val: str
    _kind: str

    def __init__(self,
                 val: str,
                 kind: str) -> None:

        self._val = val
        self._kind = kind

    def value(self) -> str:

        return self._val

    def is_number(self) -> bool:

        return self._kind == self.Number

    def is_string(self) -> bool:

        return self._kind == self.String

    def is_identifier(self) -> bool:

        return self._kind == self.Identifier

    def is_opening_paren(self) -> bool:

        return self._kind == self.OpeningParen

    def is_closing_paren(self) -> bool:

        return self._kind == self.ClosingParen

    def is_opening_bracket(self) -> bool:

        return self._kind == self.OpeningBracket

    def is_closing_bracket(self) -> bool:

        return self._kind == self.ClosingBracket

    def is_opening_curly_brace(self) -> bool:

        return self._kind == self.OpeningCurlyBrace

    def is_closing_curly_brace(self) -> bool:

        return self._kind == self.ClosingCurlyBrace

    def is_a_literal(self) -> bool:

        return self.is_number() or self.is_string() or self.is_identifier()
