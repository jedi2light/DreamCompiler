from typing import List

from .token import Token


ALPHABETIC_CHARS = ('_', '+', '-', '*', '/')


class Lexer:

    _cursor: int
    _source: str
    _tokens: List[Token]

    def __init__(self, source: str) -> None:

        self._cursor = 0
        self._source = source
        self._tokens = []

    @property
    def tokens(self) -> List[Token]:

        if not self._tokens:
            self._lex()
        return self._tokens

    def _has_next_char(self) -> bool:

        return self._cursor < len(self._source)

    def _get_curr_char(self) -> str:

        return self._source[self._cursor]

    @staticmethod
    def _is_separator(char: str) -> bool:

        return char in (' ', '\n', '\t',)

    @staticmethod
    def _is_alpha(char: str) -> bool:

        return char.isalpha() or char in ALPHABETIC_CHARS

    @staticmethod
    def _is_paren(char: str) -> bool:

        return char in ('(', ')',)

    def _is_literal_boundary(self,
                             char: str) -> bool:

        return self._is_separator(char) or self._is_paren(char)

    def _advance(self) -> None:

        self._cursor += 1

    def _lex(self) -> None:

        while self._has_next_char():

            curr_char = self._get_curr_char()

            if self._is_separator(curr_char):
                self._advance()

            elif curr_char == '(':
                self._tokens.append(Token('(', Token.OpeningParen))
                self._advance()

            elif curr_char == ')':
                self._tokens.append(Token(')', Token.ClosingParen))
                self._advance()

            elif curr_char == "[":
                self._tokens.append(Token('[', Token.OpeningBracket))
                self._advance()

            elif curr_char == "]":
                self._tokens.append(Token(']', Token.ClosingBracket))
                self._advance()

            elif curr_char == '{':
                self._tokens.append(Token('{', Token.OpeningCurlyBrace))
                self._advance()

            elif curr_char == '}':
                self._tokens.append(Token('}', Token.ClosingCurlyBrace))
                self._advance()

            elif curr_char == '"':
                val = ''
                self._advance()
                while self._has_next_char():
                    curr_char = self._get_curr_char()
                    if curr_char == '"':
                        break
                    else:
                        val += curr_char
                    self._advance()
                    if not self._has_next_char():
                        raise Error('unmatched string token', self._cursor + 1)
                self._advance()
                self._tokens.append(Token(val, Token.String))

            elif curr_char.isdigit():
                val = curr_char
                while self._has_next_char():
                    self._advance()
                    curr_char = self._get_curr_char()
                    if self._is_literal_boundary(curr_char):
                        break
                    if not curr_char.isdigit():
                        raise Error('unmatched number token', self._cursor + 1)
                    else:
                        val += curr_char
                self._tokens.append(Token(val, Token.Number))

            elif self._is_alpha(curr_char):
                val = curr_char
                while self._has_next_char():
                    self._advance()
                    curr_char = self._get_curr_char()
                    if self._is_literal_boundary(curr_char):
                        break
                    if not curr_char.isdigit() and not self._is_alpha(curr_char):
                        raise Error('unmatched identifier token', self._cursor + 1)
                    else:
                        val += curr_char
                self._tokens.append(Token(val, Token.Identifier))


class Error(SyntaxError):

    reason: str
    position: int

    def __init__(self, reason: str, position: int):

        self.reason = reason
        self.position = position

    def __str__(self) -> str:

        return f'DreamCompiler, Lexer, SyntaxError: {self.reason} at {self.position}'
