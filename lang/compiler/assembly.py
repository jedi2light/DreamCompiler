from enum import Enum


class Assembly(Enum):

    Call = 0x00

    PushString = 0x01

    PushInteger = 0x02

    @staticmethod
    def compile_a_number_push_for(number: int) -> bytearray:

        return bytearray([Assembly.PushInteger.value, number])

    @staticmethod
    def compile_a_string_push_for(string: str) -> bytearray:

        return bytearray([Assembly.PushString.value, len(string)]) + bytearray(string, 'utf-8')

    @staticmethod
    def compile_a_function_call_for(function_name: str) -> bytearray:

        return bytearray([Assembly.Call.value, len(function_name)]) + bytearray(function_name, 'utf-8')
