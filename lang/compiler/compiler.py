from lang.node import Node


class Compiler:

    _root: Node = None
    _bytecode: bytes = None

    def __init__(self, root: Node) -> None:

        self._root = root

    @property
    def bytecode(self) -> bytes:

        if not self._bytecode:
            self._compile()
        return self._bytecode

    def _compile(self) -> None:

        self._bytecode = self._root.compile()
